\chapter{LED, fotodiodi e fotoresistenze}

Prima di cominciare questo capitolo, ci segnamo un cambio di strategia operativa deciso a seguito del \textit{feedback} ricevuto riguardo l'organizzazione del logbook: segnare con un apposita sintassi le acquisizioni di dati.
\begin{acquis}{esempio.vi}
oversampling & 2\\
$R_1$ & $\SI{27}{k\Omega}$\\
\end{acquis}

\section{Light emitting diode (LED)}
\begin{exlist}
    \ok Es. 1
    \ok Es. 2
    \ok Es. 3
    \ok Es. 4
    \ok Es. 5
    \ok Es. 6
    \ok Es. 7
    \ok Es. 8
    \ok Hw. 1
\end{exlist}
\subsection{Richiami di teoria}
\paragraph{Equazione di Shockley} L'intensità di corrente $I$ che passa attraverso un diodo è ben descritta, per piccole correnti dall'equazione di shockley:
\begin{equation}
    I = I_s \left( e^{\frac{\Delta V}{nV_T}} - 1\right)
    \label{sett07:Schols04}
\end{equation}
$I_S$: intensità di corrente di saturazione; $\Delta V$: differenza di potenziale ai capi del diodo; $n$: parametro costruttivo (circa 2 per i diodi al Silicio, circa 1 per quelli al Germanio e per i LED); $V_T=k_BT/e$.
\paragraph{Energia di un fotone}
Relazione tra la lunghezza d'onda del fotone e l'energia convertita:
\begin{equation}
    \Delta E = \frac{hc}{\lambda}   
    \label{sett07:energf8n}
\end{equation}
Volendo, si può ricordare che $hc\approx \SI{1240}{eV/nm}$. Ad esempio, per un fotone con $\lambda = \SI{470}{nm}$ si ottiene $\Delta E = \SI{2.64}{eV}$


\subsection{Caratteristica I-V del diodo}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth]{sett07/LED/LED.png}
    \caption{Schema del circuito realizzato}
    \label{sett07:8wbP2Hut}
\end{figure}
Per poter impiegare il led rosso \texttt{HLMP-C115} (colore \texttt{DH AS AlGaAs}) è necessario limitare la corrente che scorre sotto i $\SI{30}{mA}$ (vedi \textit{maximum ratings} nei fogli di specifica). Dato che il circuito in figura \ref{sett07:8wbP2Hut} viene alimentato con l'uscita analogica della scheda National (voltaggio massimo: $V_\mathrm{max}=\SI{10}{V}$) è necessaria una resistenza in serie al LED di almeno $V/I=\SI{10}{V}/\SI{30}{mA}\approx\SI{330}{\Omega}$. Per sicurezza ne impieghiamo una di $\SI{470}{\Omega}$, con la quale la massima corrente che può arraversa il LED è di $I_\mathrm{max}=V_\mathrm{max}/R=\SI{10}{V}/\SI{470}{\Omega}\simeq\SI{21}{mA}$.
\begin{acquis}{Vin\_Vout\_DCDIFF.vi}
Fondo Scala AI0 &  5V\\
Fondo Scala AI1 & 5V\\
$R$ & $\SI{470}{\Omega}$\\
\end{acquis}




Abbiamo verificato tramite un processo di dicotomia con il VI \texttt{Vin\_Vout\_DCDIFF} che la luce del LED comincia ad essere visibile per $V\ped{in}\approx\SI{1.30}{V}$. Inoltre abbiamo osservato che la zona da cui esce la luce è una piastrina quadrata.

Nel calcolare la corrente che scorre nel diodo non abbiamo proceduto ad analizzare valori oltre i $\SI{5}{V}$ in quanto abbiamo visto che il limite di corrente dell'analog output (da datasheet $\SI{5}{mA}$) impedisce alla scheda di fornire tensioni che comportino correnti oltre i $\sim \SI{10}{mA}$. ($V=RI=\SI{470}{\Omega}\cdot\SI{0.010}{A}\simeq5V$).
\begin{acquis}{Traccia\_Vin\_Vout\_diff.vi}
Fondo Scala AI0 &  5V\\
Fondo Scala AI1 & 5V\\
$R$ & $\SI{470}{\Omega}$\\
Vmin & 0.0V \\
Vmax & 4.0V \\
Numero misure & 100 \\
\end{acquis}
\begin{figure}[H]
    \centering
    \subfloat[][Caratteristica I-V]{\includegraphics[width=0.48\textwidth]{sett07/LED/I-V.png}}
    \subfloat[][Caratteristica I-V]{\includegraphics[width=0.48\textwidth]{sett07/LED/I-V_semilogy.png}}
    \caption{Caratteristica I-V}    \label{sett07:Gv72wFtJ}
\end{figure}
$\Delta V$ è la differenza di potenziale ai capi del diodo.
\subsection{Misura di $h$}
\subsubsection{Introduzione teorica}
Un LED è ragionevolmente approssimato da Schockley (equazione \ref{sett07:Schols04}) per basse correnti. Per tecnologie di fabbricazione simili, possiamo confrontare le correnti di saturazione $I_s$ e affermare che, in prima approssimazione, queste sono proporzionali alla densità dei portatori e all'area della giunzione. La densità dei portatori di carica in prossimità della giunzione è ben descritta dalla distribuzione di Boltzmann, per cui se il gap energetico tra le bande in prossimità della giunzione è $E_g = eV_g$, la densità dei portatori sarà proporzionale a $e^{-E_g/kT}=e^{-V_g/V_T}$, con $V_T=kT/e$. È quindi giustificato scrivere $I_s=I^*(T,A)e^{-V_g/V_T}$, dove $I^*(T,A)$ è una funzione che dipende da $A$, l'area della giunzione, e $T$, la temperatura. Se i diodi sono sufficientemente simili da un punto di vista costruttivo, possiamo supporre che $A$ non vari e quindi, a parità di temperatura, si ha che $I_s\propto e^{-V_g/V_T}$. Riprendendo Schockley possiamo quindi scrivere:
\begin{equation}
    I=I_se^{V/V_T}\propto e^{(V-V_g)/V_T}
    \label{sett07:sdfvsd02}
\end{equation}
In altre parole, all'interno di una famiglia di LED in cui vale la stessa costante di proporzionalità (AKA $I^*(T,A)$ è costante al variare del LED), se fissiamo $I$ allora fissiamo anche $V-V_g$. In altro modo, detta $\alpha$ una generica costante di proporzionalità tra $I$ e $e^{(V-V_g)/V_T}$ all'interno di una famiglia di LED, si ha:
\[I=\alpha e^{(V-V_g)/V_T}\Rightarrow V-V_g=V_T\log(I/\alpha)\Rightarrow \]
\[ \Rightarrow eV-E_g=kT\log(I/\alpha) \Rightarrow \frac{hc}{\lambda}=eV-kT\log(I/\alpha) \Rightarrow \]
\begin{equation}
    \Rightarrow \frac{1}{\lambda}=\frac{e}{hc}V-\frac{kT}{hc}\log(I/\alpha)
\end{equation}
Se quindi fissiamo $I$ otteniamo una relazione lineare tra $1/\lambda$ e $V$, ottenibile variando il LED. Detto $m$ il coefficiente angolare in particolare si ottiene $h=e/mc$.


\subsubsection{Dati raccolti e analisi dati}

\begin{acquis}{Traccia\_Vin\_Vout\_diff.vi}
Fondo Scala AI0 &  5V\\
Fondo Scala AI1 & 5V\\
$R$ & $\SI{4.70}{k\Omega}$\\
Vmin & 0.0V \\
Vmax & 4.0V \\
Numero misure & 200 \\
\end{acquis}
Abbiamo ripetuto l'acquisizione per diversi LED (vedi tabella \ref{settXX:uHEe2Ny8}), ottenendo il grafico IV di figura \ref{sett07:Y2fsAzwc}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{sett07/LED/led-IV.png}
    \caption{Grafico I-V dei 6 LED considerati}
    \label{sett07:Y2fsAzwc}
\end{figure}

Abbiamo fissato la corrente ad un valore di $\SI{10}{\mu A}$ e abbiamo costruito la tabella \ref{settXX:uHEe2Ny8}, abbiamo poi fittato $1/\lambda$ in funzione di $V\ped{out}$ con una retta. Il coefficiente angolare $m$, per quanto visto in precedenza, è tale che $h=e/(mc)$. In tal modo abbiamo ottenuto una stima per la costante di Planck: $h=\SI{10\pm3e-34}{kg\,m^2\,s^{-1}}$.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline
 LED usato & V [V] a $\SI{10}{\mu A}$ & $\lambda$ [nm] & $\Delta\lambda$ [nm] & Colore \\
\hline
    LY3336 &                     2.34 &          587.0 &     +8\hspace{5pt}-7 & Giallo \\
    LO3336 &                     1.56 &          606.0 &     +3\hspace{5pt}-6 & Arancione \\
   SFH4873 &                     1.12 &          880.0 &              $\pm$30 & Infrarosso \\
  TLHG4405 &                     1.75 &          565.0 &    +10\hspace{5pt}-3 & Verde \\
    LB3333 &                     2.49 &          471.0 &               $\pm$6 & Blu \\
 HLMP-C115 &                     1.51 &          645.0 &              $\pm$10 & Rosso \\
\hline
\end{tabular}
\caption{Led usati, lenghezze d'onda e ddp a $\SI{10}{\mu A}$}
\label{settXX:uHEe2Ny8}
\end{table}
Abbiamo poi scelto cinque valori della corrente da usare per stimare il valore di $h$, ottenendo i risultati della tabella \ref{settXX:R6KJu4OK}.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|}
\hline
$I$ [$\mu$A] & $h$ [$\SI{e-34}{kg\,m^2\,s^{-1}}$]\\
\hline
   1.00 &     10.1$\pm$3.3 \\
  3.76 &     10.5$\pm$3.3 \\
  14.1 &     10.6$\pm$3.3 \\
  53.2 &     10.5$\pm$3.3 \\
   200 &     10.5$\pm$3.2 \\
\hline
\end{tabular}
\caption{Stime di $h$ ottenute per diversi valori di $I$ con i 6 LED}
\label{settXX:R6KJu4OK}
\end{table}

Osservando il grafico di figura \ref{sett07:Y2fsAzwc} abbiamo notato che il led giallo è particolarmente ``sfasato'' rispetto agli altri LED, dato che ci aspetteremmo di trovarlo in prossimità del LED verde sul grafico. Abbiamo quindi ipotizzato che si trattasse di una tecnologia di fabbricazione diversa e abbiamo provato a escluderlo, ottenendo il grafico di figura \ref{sett07:pJvtLQ5d} e la tabella \ref{sett07:J2Fd7nse}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{sett07/LED/led-IV-withIvaluesForH-noY.png}
    \caption{Grafico I-V senza il LED giallo con segnati i valori di corrente scelti per la stima di $h$}
    \label{sett07:pJvtLQ5d}
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{|c|c|}
\hline
$I$ [$\mu$A] & $h$ [$\SI{e-34}{kg\,m^2\,s^{-1}}$]\\
\hline
   1.00 &      7.00$\pm$0.67 \\
  3.76 &     7.57$\pm$0.93 \\
  14.1 &     7.71$\pm$1.03 \\
  53.2 &     7.68$\pm$1.04 \\
   200 &     7.68$\pm$1.04 \\
\hline
\end{tabular}
\caption{Stime di $h$ ottenute per diversi valori di $I$ escludendo il led giallo}
\label{sett07:J2Fd7nse}
\end{table}

E a questo punto ci siamo detti: perché limitarci a cinque valori di corrente? Perché non ne prendiamo \textit{tanti}, così da prendere in sostanza un grafico di una stima di $h$ in funzione di $I$? E così abbiamo ottenuto il grafico di figura \ref{sett07:A9N7b5tc}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{sett07/LED/h-vs-I-noY-withUncertainties.png}
    \caption{Stima di $h$ \textit{vs} $I$, escludendo il LED giallo nella stima. In nero la stima ottenuta, in azzurro la regione all'interno del range dato dall'incertezza sulla stima. In rosso il valore corretto di $h$.}
    \label{sett07:A9N7b5tc}
\end{figure}


Il valore di $h$, definito esatto dal 20 Maggio 2019, è $\SI{6.62607015e-34}{J\,s}$.
\subsubsection{Script utilizzato}
\begin{minted}{octave}
clear all;
pkg load optim
warning("off");


files{1} = './../data/es5_SFH4873';
files{2} = './../data/es5_LO3336';
files{4} = './../data/es5_HLMP-C115';
files{3} = './../data/es5_TLHG4405';
files{5} = './../data/es5_LB3333';
files{6} = './../data/es5_LY3336'; % è strano, si può commentare

color{1} = 'k'; % black, infrared
color{2} = [1 0.5 0]; % should be orange
color{4} = 'r'; % red
color{3} = 'g'; % green
color{5} = 'b'; % blue
color{6} = [0.9 0.9 0]; % yellow - è strano, si può commentare

names{1} = 'SFH4873';
names{2} = 'LO3336';
names{4} = 'HLMP-C115';
names{3} = 'TLHG4405';
names{5} = 'LB3333';
names{6} = 'LY3336'; % è strano, si può commentare

lambda(1) = 880;
lambda(2) = 606;
lambda(4) = 645;
lambda(3) = 565;
lambda(5) = 471;
lambda(6) = 587; % è strano, si può commentare
lambda = lambda./1e9; %lambda in meters

%% DEFINIZIONI COMODE

c = 299.8e6;
e = 1.602e-19;
function yy = straight_line(param, xx)
	yy = param(1).*xx+param(2);
end

%% GRAFICO SENZA STIMA DI H

figure(1);
for i = 1:length(files)
	fID = fopen(files{i});
	data = fscanf(fID, '%f %f', [2, inf]);
	fclose(fID);
	data = data';
	V = data(:,1);
	I = data(:,2).*1e3;
	semilogy(V, abs(I), "Color", color{i}, "LineWidth", 2);
	hold on;
end

h = legend(names);
legend (h, "location", "northwest");
xlim ([0,2.7]);
xlabel ("V [V]", "fontsize", 16);
ylabel ("I [uA]", "fontsize", 16);
set (h, "fontsize", 16);
set (gca, "FontSize", 16);

%% GRAFICO DI N STIME DI H

N = 5
Ilin = logspace(log10(1),log10(200),N); % dipende dalle unità di misura
Hest = Ilin;
dHest = Ilin;

figure(2);
for i = 1:length(files)
	fID = fopen(files{i});
	data = fscanf(fID, '%f %f', [2, inf]);
	fclose(fID);
	data = data';
	V = data(:,1);
	I = data(:,2).*1e3;
	semilogy(V, abs(I), "Color", color{i}, "LineWidth", 2);
	hold on;
end

for j = 1:length(Ilin)
	Vlin = linspace(0,2.7,100);
	plot(Vlin,ones([1, length(Vlin)]).*Ilin(j),'k');
	Vext = ones([1, length(files)]);
	for i = 1:length(files)
		fID = fopen(files{i});
		data = fscanf(fID, '%f %f', [2, inf]);
		fclose(fID);
		data = data';
		V = data(:,1);
		I = data(:,2).*1e3;
		indexlower = sum(I<Ilin(j));
		indexupper = length(I)-sum(I>Ilin(j))+1;
		lowerweight = 1/abs(I(indexlower)-Ilin(j));
		upperweight = 1/abs(I(indexupper)-Ilin(j));
		Vext(i) = (V(indexlower)*lowerweight+V(indexupper)*upperweight);
		Vext(i) = Vext(i) / (lowerweight+upperweight);
	end
	VextTable = [1./lambda; Vext]';
	X = Vext; Y = 1./lambda;
	[popt,R,J,CovB,MSE,ErrorModelInfo] = nlinfit(X, Y, @straight_line, [1, 1]);
	m = popt(1);
	q = popt(2);
	dm = sqrt(CovB(1,1));
	Hest(j) = e/m/c;
	dHest(j) = dm*e/c/m^2;
end


h = legend(names);
legend (h, "location", "northwest");
N = 1000
xlabel ("V [V]", "fontsize", 16);
ylabel ("I [uA]", "fontsize", 16);
set (h, "fontsize", 16);
set (gca, "FontSize", 16);

HestTable = [Ilin; Hest; dHest]'


%% GRAFICO DI H IN FUNZIONE DI I

N = 1000
Ilin = logspace(log10(0.3),log10(200),N); % dipende dalle unità di misura
Hest = Ilin;
dHest = Ilin;

figure(3);

for j = 1:length(Ilin)
	Vlin = linspace(0,2.7,100);
	Vext = ones([1, length(files)]);
	for i = 1:length(files)
		fID = fopen(files{i});
		data = fscanf(fID, '%f %f', [2, inf]);
		fclose(fID);
		data = data';
		V = data(:,1);
		I = data(:,2).*1e3;
		indexlower = sum(I<Ilin(j));
		indexupper = length(I)-sum(I>Ilin(j))+1;
		lowerweight = 1/abs(I(indexlower)-Ilin(j));
		upperweight = 1/abs(I(indexupper)-Ilin(j));
		Vext(i) = (V(indexlower)*lowerweight+V(indexupper)*upperweight);
		Vext(i) = Vext(i) / (lowerweight+upperweight);
	end
	VextTable = [1./lambda; Vext]';
	X = Vext; Y = 1./lambda;
	[popt,R,J,CovB,MSE,ErrorModelInfo] = nlinfit(X, Y, @straight_line, [1, 1]);
	m = popt(1);
	q = popt(2);
	dm = sqrt(CovB(1,1));
	Hest(j) = e/m/c;
	dHest(j) = dm*e/c/m^2;
end


ErrorBar = errorbar(Ilin, Hest, dHest);
set(ErrorBar, 'Color', [0.5 0.6 1], 'linewidth', 2);
set(ErrorBar, 'Marker', '.', 'MarkerSize', 10);
set(ErrorBar, 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');
set(gca,'xscale','log');
hold on;
plot(Ilin, ones(size(Ilin))*6.626e-34,'r', "linewidth", 3);
xlim ([min(Ilin),max(Ilin)]);
xlabel ("I [uA]", "fontsize", 16);
ylabel ("h [kg m^2 s^{-1}]", "fontsize", 16);
set (h, "fontsize", 16);
set (gca, "FontSize", 16);
\end{minted}
\section{Fotodiodo}
\begin{exlist}
    \ok Hw. 2
    \ok Es. 9
    \ok Es. 10
    \ok Es. 11
    \ok Es. 12
    \ok Hw. 3
\end{exlist}
Tra le grandezze principali che possiamo trovare nel documento \textit{Photodiode technical Infromation} dell'azienda Giapponese Hamamatsu riguardanti i fotodiodi prodotti, che compaiono usualmente nei \textit{datasheet} e che si sarebbein grado di misurare in laboratorio troviamo:
\begin{itemize}
    \item Sensibilità (o responsività): misurata nel seguito (insieme all'efficenza quantica)
    \item Corrente di buio: corrente che scorre nel fotodiodo oscurato, misurabile oscurando completamente il fotodiodo e applicandoci un voltaggio inverso.
    \item Capacità: misurabile tramite un semplice filtro RC con $R=\SI{1}{M\Omega}$ e una frequenza di sampling della DAQ di $\SI{100}{ks^{-1}}$.
    \item Tempo di salita: tempo per passare dal 10\% al 90\% del livello di output stazionario. (Misurato nel seguito).
\end{itemize}
\begin{equation}
    I_\mathrm{lux}=\eta\frac{We}{h\nu}
\end{equation}
Dal foglio-dati del fotodiodo al Silicio \texttt{OSD15-5T} troviamo tra i dati maggiormente significativi: (non so, magari gli absolute maximum ratings)
\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|c|}
     \hline
     Responsivity [A/W] & Dark Current [nA] & Capacitance [pF]\\
     \hline
     Min:0.15 Typ:0.21 & Max:50 Typ:3 & 390\\
     \hline
    \end{tabular}
    \caption{Dati \texttt{OSD15-5T}}
    \label{sett07:myvlabel}
\end{table}

\subsection{Voltaggio a circuito aperto}
Connettendo i terminali del fotodiodo rivolto verso l'alto ad un tester in configurazione di voltmetro misuriamo $V_\mathrm{OC}=\SI{0.34\pm0.03}{V}$ (\textit{Voltage Open Circuit}). Sul \textit{datasheet} non è riportata esplicitamente $V_\mathrm{OC}$, tuttavia si può ricavare e risulta essere: (eventualmente mettere i conti)
\[V_\mathrm{OC}=0.21V,\;\eta=1 \qquad 
V_\mathrm{OC}=0.38V,\;\eta=1.8\]
In questo regime fotovoltaico la risposta non è lineare:
\begin{equation}
    I=I_s\left(e^{V/V_T}-1\right)-I_\mathrm{lux}
\end{equation}
Si tratta infatti dell'equzione di Shockley traslata.

Per ottenere un voltaggio positivo all'ingresso \texttt{COM} del voltmetro va collegato il catodo del fotodiodo.

\subsection{Corrente di cortocircuito}
Condizione: altezza del tavolo, rivolto verso la lampada che sta sopra.
$I_\mathrm{SC}=\SI{5\pm1}{\mu A}$
\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|c|}
    \hline
    Gruppo  &   $V_\mathrm{OC}$ [V] &   $I_\mathrm{SC}$ [$\mu$A] \\
    \hline
    FALMAL  &   $0.34\pm0.03$   &   $5\pm1$ \\
    VASPER  &   $0.336\pm1$     & $4.06\pm0.05$ \\
    \hline
    \end{tabular}
    \caption{Caption}
    \label{sett07:vsdn9v6c}
\end{table}
Analogamente ai conti effettuati prima, dal datasheet otteniamo $I_\mathrm{SC}=\SI{15}{\mu A}$. (È un logaritmo...)

Al fine di ottenere una corrente positiva è stato collegato il catodo (polo negatico) del fotodiodo all'ingresso \texttt{COM} dell'amperometro.

\subsection{Segnali acquisiti con DAQ e amplificatore a transresistenza}
Colleghiamo l'analog input della DAQ all'output dell'opamp.
\begin{obs}
La corrente alternata a $f_\mathrm{ENEL}=\SI{50}{Hz}$ nelle lampade a fluorescenza implica che si hanno dei picchi luminosi in corrisponenza del valore massimo del modulo della corrente a frequenza pari a $f=\SI{100}{Hz}$.
\end{obs}

\subsubsection{Acquisizione normale}
\begin{acquis}{acquis\_base\_6x21.vi}
Fondo Scala [V] & 1 \\
Input terminal configuration & RSE \\
sample mode & finite samples \\
durata (s) & 0.1 \\
campioni al secondo & 100000\\
\end{acquis}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{sett07/Fotodiodo/Fotodiodo.png}
    \caption{Risposta del fotodiodo in condizioni standard.}
    \label{sett07:uk2EC8Zy}
\end{figure}
Si osseva un segnale periodico di periodo $\SI{0.01}{s}$ (si contano 10 picchi in $\SI{0.1}{s}$), quindi di frequenza pari a $f=\SI{100}{Hz}$, esattamente come ci aspettavamo dall'osservazione precedente.

\subsubsection{Acquisizione focalizzata}
\begin{acquis}{acquis\_base\_6x21.vi}
Fondo Scala [V] & 5 \\
Input terminal configuration & RSE \\
sample mode & finite samples \\
durata (s) & 0.1 \\
campioni al secondo & 100000\\
\end{acquis}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{sett07/Fotodiodo/Fotodiodo_focalizzato.png}
    \caption{Risposta del fotodiodo sotto luce focalizzata.}
    \label{sett07:4gkhuWZ2}
\end{figure}
Per quanto riguarda la frequenza del segnale osserviamo lo stesso risultato ottenuto in precedenza. Come atteso invece abbiamo un incremento dell'ampiezza del segnale prodotto.

\subsubsection{Acquisizione buio}
\begin{acquis}{acquis\_base\_6x21.vi}
Fondo Scala [V] & 0.2 \\
Input terminal configuration & RSE \\
sample mode & finite samples \\
durata (s) & 0.1 \\
campioni al secondo & 100000\\
\end{acquis}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{sett07/Fotodiodo/Fotodiodo_schermato.png}
    \caption{Risposta del fotodiodo oscurato.}
    \label{sett07:9EfsSusd}
\end{figure}
Come aspettato l'ampiezza è molto piccola e la frequenza del segnale non ben determinata.

Ricordandoci che il voltaggio in uscita da un amplificatore a transresistenza segue la legge $V_\mathrm{out}=-RI$ abbiamo che la corrente generata dal fotodiodo vale $I_\mathrm{ph}=-\frac{V_\mathrm{out}}{R}$, con modulo uguale alla media integrale (presa a occhio) diviso il valore della resistenza ($\SI{100}{k\Omega}$).
\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|c|c|}
    \hline
     & Standard & Focalizzato & Oscurato\\
    \hline
    V medio [V] & -0.5 & -1.4 & $2.5\times10^{-3}$\\
    $I_\mathrm{ph} $ [$\mu$A] & 5 & 14 & 0.025\\
    \hline
    \end{tabular}
    %\caption{Caption}
    \label{sett07:my4bc9el}
\end{table}
La responsività rappresenta la conversione tra corrente gererata e flusso luminoso, nel nostro caso abbiamo un valore tipico di $\SI{0.21}{A/W}$.

\section{Fotodiodo e LED}
\begin{exlist}
    \ok Es. 13
    \ok Es. 14
    \ok Es. 15
    \ok Es. 16
    \ok Es. 17
    \ok Hw. 4
    \ok Es. 18
    \ok Es. 19
    \ok Es. 20
    \ok Hw. 5
    \ok Es. 21
    \ok Es. 22
    \ok Es. 23
    \ok Es. 24
    \ok Hw. 6
    \todo Es. 25 - MANCANO DATI
\end{exlist}

\subsection{Regime fotoconduttivo}

\subsubsection{Preliminari}

Vogliamo ora lavorare con un LED che illumini il fotodiodo, il quale deve lavorare in regime fotoconduttivo.

Abbiamo scelto un LED con una ragionevole regione abbastanza lineare tra la corrente $I$ e la potenza irraggiata $P$: l'\texttt{HLMP-C115}, da utilizzare preferibilmente per correnti nel range 1-20mA. Per poter lavorare senza l'eccessiva limitazione dei 5 mA della DAQ abbiamo montato un circuito inseguitore con un op-amp \texttt{uA741CP}.

Abbiamo quindi diretto la luce del LED il più possibile verso il fotodiodo, avvicinando a contatto i due dispositivi.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth]{sett07/LEDandFotodiodo/hlmpc115-Lumin-vs-I.png}
    \caption{Grafico della potenza irraggiata in funzione della corrente. L'\texttt{HLMP-C115} è del tipo \texttt{DH AS AlGaAs}}
    \label{sett07:rnd8gGbI}
\end{figure}

\subsubsection{$I\ped{PD}$ \textit{vs} $I\ped{LED}$}

\begin{acquis}{PD\_LED.vi}
Vmin (V) & 3\\
Vmax (V) & 10\\
Nmis & 20\\
R\_LED (kOhm) & 0.4700\\
R\_tra (kOhm) & -47.00\\ %% segni messi in modo che venga tutto sugli assi positivi
Fondo scala CB68 [V] & 10 \\
Fondo scala CB [V] & 10 \\
\end{acquis}
Purtroppo Nmis = 20 perché dovevamo misurare velocemente, dato che tenevamo il fotodiodo appoggiato al LED con le mani. Abbiamo ottenuto il grafico \ref{sett07:EVWjhc4M} (nota che saturiamo la corrente che può fornire il circuito inseguitore).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{sett07/LEDandFotodiodo/IPDvsILED.png}
    \caption{Grafico di $I\ped{PD}$ vs $I\ped{LED}$}
    \label{sett07:EVWjhc4M}
\end{figure}

Abbiamo poi costruito un grafico di $P\ped{LED}$ vs $I\ped{LED}$. Per ottenere $P\ped{LED}$ siamo partiti da $I\ped{PD}$. Prima abbiamo calcolato la responsività alla lunghezza d'onda $\lambda_2$ usando la responsività $\chi_1$, fornita dal datasheet, relativa alla lunghezza d'onda $\lambda_1$: $\chi_2=\chi_1*\lambda_2/\lambda_1$ (tale espressione è valida solo se il fotodiodo cattura bene anche la luce a lunghezza d'onda $\lambda_2$). Una volta ottenuto $\chi_2$ abbiamo calcolato $P\ped{LED}=I\ped{PD}*\chi_2$. Il grafico ottenuto è riportato in figura \ref{sett07:xVt7PZVu}.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{sett07/LEDandFotodiodo/PLEDvsILED.png}
    \caption{Grafico di $P\ped{LED}$ vs $I\ped{LED}$}
    \label{sett07:xVt7PZVu}
\end{figure}

\subsubsection{$I\ped{PD}$ \textit{vs} distanza}
Abbiamo acquisito $I\ped{PD}$al variare della distanza tra LED e fotodiodo. Riportiamo i dati in tabella \ref{settXX:JtIM7oVR}; il LED si trova con la capocchia al livello della fila 55 della breadboard. Per ottenere la distanza in mm, basta ricordare che la distanza tra due file adiacenti è di 0.1 pollici, ovvero $\SI{2.54}{mm}$.
\begin{acquis}{Vin\_Vout\_2C.vi}
V in volt & 10.00 \\
Fondo scala AI0 [V] & 10 \\
Fondo scala AI1 [V] & 10 \\
V1 medio (aka il voltaggio ai capi della resistenza in serie al LED) & (5.939$\pm$0.002)V\\
$R\ped{tra}$ & $\SI{47.0}{k\Omega}$\\
\end{acquis}

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
 V0 [V] & posizione [pt bread] \\
\hline
 7.928$\pm$0.006 &                 55 \\
 3.254$\pm$0.007 &                 50 \\
 1.555$\pm$0.009 &                 45 \\
 0.892$\pm$0.005 &                 40 \\
 0.626$\pm$0.008 &                 35 \\
 0.453$\pm$0.006 &                 30 \\
 0.354$\pm$0.005 &                 25 \\
 0.292$\pm$0.005 &                 20 \\
 0.246$\pm$0.005 &                 15 \\
 0.220$\pm$0.008 &                 10 \\
 0.210$\pm$0.007 &                  5 \\
 0.201$\pm$0.017 &                  0 \\
\hline
\end{tabular}
\caption{Dati raccolti: V in uscita dall'amplificatore a transresistenza \textit{vs} posizione}
\label{settXX:JtIM7oVR}
\end{table}

Considerando che il fotodiodo può pigliare un massimo di luce (tutto il LED + ambiente) e un minimo c'è sempre, dato che l'acquisizione non era coperta, e quindi c'è la luce ambientale, abbiamo fittato i dati con il modello:
\begin{equation}
    P\ped{PD} = \min\left(\texttt{maximum}\,,\,\texttt{coefficient}\frac{1}{(X\ped{mm}-\texttt{offset})^2} + \texttt{background}\right);
\end{equation}

Abbiamo ottenuto per i parametri le seguenti stime: \\
\texttt{maximum} = $\SI{0.0499}{mW}$\\
\texttt{coefficient} = $\SI{14.0}{mW \,mm^2}$\\
\texttt{offset} = $\SI{153.5}{mm}$\\
\texttt{background} = $\SI{5.56e-04}{mW}$\\

Riportiamo in figura \ref{sett07:DmNbM877} un confronto tra i dati raccolti e il grafico del fit.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{sett07/LEDandFotodiodo/PPDvsPosition.png}
    \caption{Grafico di $P\ped{PD}$ \textit{vs} $X\ped{mm}$}
    \label{sett07:DmNbM877}
\end{figure}


\subsection{Regime fovoltaico}
\subsubsection{Recap teorico}
Considerando il fotodiodo come un diodo con asse delle correnti traslato a causa dell'illuminazione, possiamo richiedere che $I=0$ nell'equazione:
\begin{equation}
    I = I_s(e^{V/nV_T}-1)-I\ped{lux}
\end{equation}
Se assumiamo $I\ped{lux}\propto I\ped{LED}$, detta $\alpha$ una generica costante di proporzionalità:
\begin{equation}
    V\ped{OC} = nV_T \ln(1+\alpha I\ped{LED})
    \label{sett07:dfghjkuv}
\end{equation}
Risulta quindi interessante verificare questa relazione tra $V\ped{OC}$ e $I\ped{LED}$.

\subsubsection{Preliminari}
Abbiamo staccato il fotodiodo dall'amplificatore a transresistenza, per misurarne direttamente la ddp ai capi, utilizzandolo quindi in regime fotovoltaico.
\begin{acquis}{Vin\_Vout\_2C.vi}
V in volt & 10.00 \\
Fondo scala AI0 [V] & 10 \\
Fondo scala AI1 [V] & 10 \\
V1 medio (aka il voltaggio ai capi della resistenza in serie al LED) & (5.929$\pm$0.002)V\\
$R\ped{LED}$ & $\SI{470}{\Omega}$\\
\end{acquis}
Abbiamo ripetuto la precedente acquisizione in diverse condizioni di luce, tenendo fissata la corrente che scorre nel led a $\SI{12.6}{ mA}$.
\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|}
        \hline
        Condizione di luce & $V\ped{PD}$ [mV] \\
        Buio ambientale, con led accanto & 431.6$\pm$0.2\\
        Buio totale & 303.1$\pm$1.5\\
        Luce ambientale, senza led accanto & 330.9$\pm$4.2\\
        Luce ambientale, con led accanto & 443.91$\pm$0.2\\
        \hline
    \end{tabular}
    \caption{Dati raccolti in diverse condizioni di illuminazione del fotodiodo}
    \label{sett07:v8Q6CUsV}
\end{table}

\subsubsection{$V\ped{OC}$ \textit{vs} $I\ped{LED}$}


Abbiamo poi fatto un supporto meccanico con i cavi per assicurare la stabilità dell'apparato (d'ora in avanti ci riferiremo a questo semplicemente come \textit{il serpente}) e abbiamo coperto con i panni degli occhiali:
\begin{acquis}{PD\_LED.vi}
Vmin (V) & 3\\
Vmax (V) & 10\\
Nmis & 20\\
R\_LED (kOhm) & 0.4700\\
R\_tra (kOhm) & 1\\ %% segni messi in modo che venga tutto sugli assi positivi
Fondo scala CB68 [V] & 10 \\
Fondo scala CB33 [V] & 10 \\
\end{acquis}

Abbiamo quindi preso i dati raccolti e li abbiamo fittati con la \ref{sett07:dfghjkuv}, lasciando non solo $\alpha$ ma anche $nV_T$ come parametro libero, ottenendo come stime:\\
\texttt{$nV_T$} = $\SI{45.3}{mV}$\\
\texttt{$\alpha$} = $\SI{1200}{mA^{-1}}$\\

Riportiamo in figura \ref{sett07:Mpd8aQ5f} il confronto tra dati raccolti e fit.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{sett07/LEDandFotodiodo/VOCvsILED.png}
    \caption{Relazione tra $V\ped{OC}$ e $I\ped{LED}$: dati sperimentali e fit}
    \label{sett07:Mpd8aQ5f}
\end{figure}
\subsection{Tempi di risposta}
Come descritto nel testo fornito dalla \textit{Hamamatsu}, il \textit{risetime} di un fotodiodo è il tempo necessario affinché il segnale di $V\ped{OC}$ salga dal 10\% al 90\% del valore che si ha in condizioni stazionarie. Descrive, quindi, la velocità di risposta del fotodiodo. 

Il circuito costruito per analizzare il \textit{risetime} consiste in un led alimentato dal generatore di funzioni e dal fotodiodo, i cui elettrodi sono collegati da una resistenza $R$.

Ci attendiamo che una resistenza troppo grande alteri il processo di misura, in quanto, per tensioni ragionevolmente raggiungibili dal fotodiodo, la corrente che scorre nel circuito non dipende da $R$, quindi $V\propto R$. In altre parole, aumentare la resistenza vuol dire richiedere voltaggi maggiori, che corrisponde ad attendere che un maggior numero di cariche si accumuli agli elettrodi del fotodiodo. Risulta quindi chiaro che una maggiore resistenza implichi tempi di risposta grandi.

Abbiamo quindi concentrato le nostre misure sull'output del fotodiodo, per alcuni fissati valori di $R$.


\begin{acquis}{acquis\_base\_6x21.vi}
Fondo Scala [V] & 1 \\
Input terminal configuration & RSE \\
sample mode & finite samples \\
durata (s) & 0.002 \\
campioni al secondo & 250000\\
frequenza del generatore & 1kHz\\
ampl generatore & 6V\\
offset generatore & 3V\\
$R$ & 10k$\Omega$\\
\end{acquis}

In questo caso, dal grafico si ha che RISE TIME $\approx$ 0.075ms

Abbiamo provato ad aumentare il valore di $R$ a 100k$\Omega$.
\begin{acquis}{acquis\_base\_6x21.vi}
Fondo Scala [V] & 1 \\
Input terminal configuration & RSE \\
sample mode & finite samples \\
durata (s) & 0.01 \\
campioni al secondo & 250000\\
frequenza del generatore & 200Hz\\
ampl generatore & 6V\\
offset generatore & 3V\\
$R$ & 100k$\Omega$\\
\end{acquis}

In questo caso, dal grafico si ha che RISE TIME $\approx$ 0.14ms. Annotiamo inoltre che in questo caso siamo andati in saturazione del voltaggio disponibile dal fotodiodo ($V\ped{OC}$).

\begin{figure}[H]
    \centering
    \subfloat[][$R=\SI{10}{k\Omega}$]{
    \includegraphics[width=0.48\textwidth]{sett07/LEDandFotodiodo/VPDvsT10k.png}
    }
    \subfloat[][$R=\SI{100}{k\Omega}$]{
    \includegraphics[width=0.48\textwidth]{sett07/LEDandFotodiodo/VPDvsT100k.png}
    }
    \caption{Grafici dei dati raccolti}
    \label{sett07:rd63nxeu}
\end{figure}

Vogliamo ora valutare la risposta in frequenza del circuito in una regione di alimentazione del LED dove controllare il voltaggio linearmente vuol dire controllare anche la corrente, e quindi la potenza irraggiata, linearmente.

Per determinare il regime lineare abbiamo rifatto un'acquisizione completa con 400 misure.
\begin{acquis}{Traccia\_Vin\_Vout\_diff.vi}
Fondo Scala AI0 & 10V\\
Fondo Scala AI1 & 10V\\
$R\ped{LED}$ & $\SI{0.470}{k\Omega}$\\
Vmin & 0.0V \\
Vmax & 10.0V \\
Numero misure & 400 \\
\hline
LED & HLMP-C115\\
\end{acquis}

\begin{figure}[H]
    \centering
    \subfloat[][Scala lineare]{
    \includegraphics[width=0.48\textwidth]{sett07/LEDandFotodiodo/HLMP-C115_IV_linlin.png}
    }
    \subfloat[][Scala semilogaritmica]{
    \includegraphics[width=0.48\textwidth]{sett07/LEDandFotodiodo/HLMP-C115_IV_linlog.png}
    }
    \caption{Caratteristica IV dell'\texttt{HLMP-C115}}
    \label{sett07:rd98nfUS}
\end{figure}

Abbiamo allora trovato con il \textit{VI} \texttt{Vin\_Vout\_2C.vi} i voltaggi di $V\ped{out}$ necessari per lavorare in una regione di voltaggi $V\ped{LED}$ con realzione con la corrente abbastanza lineare.

Abbiamo quindi acquisito il guadagno del circuito (qui presentato in mA/mW) per diverse frequenze.

\begin{acquis}{bode\_ph\_offset.vi}
FScala In (68) & 10V\\
FScala Out (33) & 5V\\
Ampiezza & $\SI{0.500}{V}$\\
Offset & $\SI{4.500}{V}$\\
Freqmin & 5\\
Freqmax & 8000\\
Numero misure & 100 \\
Nperiodi & 5\\
Ncamp/periodo & 25\\
Guadagno del partitore & 1.000E+0\\
\hline
Transresistenza & $\SI{47}{k\Omega}$\\
LED & \texttt{HLMP-C115} (Rosso)\\
\end{acquis}

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{sett07/LEDandFotodiodo/RossoBodeGuadagno.png}
    \caption{Diagramma di Bode del guadagno del sistema LED rosso + fotodiodo}
    \label{sett07:ecfxo565}
\end{figure}

Abbiamo ripetuto l'acquisizione cambiando transresistenza e LED.

\begin{acquis}{bode\_ph\_offset.vi}
FScala In (68) &  10V\\
FScala Out (33) & 5V\\
Ampiezza & $\SI{0.500}{V}$\\
Offset & $\SI{4.500}{V}$\\
Freqmin & 5\\
Freqmax & 8000\\
Numero misure & 200 \\
Nperiodi & 5\\
Ncamp/periodo & 8\\
Guadagno del partitore & 1.000E+0\\
\hline
Transresistenza & $\SI{82}{k\Omega}$\\
LED & \texttt{LO 3336} (Arancione)\\
\end{acquis}


Abbiamo poi provato a ripetere l'acquisizione per un altro valore della transresistenza.

\begin{acquis}{bode\_ph\_offset.vi}
FScala In (68) & 10V\\
FScala Out (33) & 5V\\
Ampiezza & $\SI{0.500}{V}$\\
Offset & $\SI{4.500}{V}$\\
Freqmin & 5\\
Freqmax & 8000\\
Numero misure & 200 \\
Nperiodi & 5\\
Ncamp/periodo & 25\\
Guadagno del partitore & 1.000E+0\\
\hline
Transresistenza & $\SI{10}{k\Omega}$\\
LED & \texttt{LO 3336} (Arancione)\\
\end{acquis}

\begin{figure}[H]
    \centering
    \subfloat[][$R\ped{trans} = \SI{82}{k\Omega}$]{
    \includegraphics[width=0.48\textwidth]{sett07/LEDandFotodiodo/ArancioBodeGuadagno.png}
    }
    \subfloat[][$R\ped{trans} = \SI{10}{k\Omega}$]{
    \includegraphics[width=0.48\textwidth]{sett07/LEDandFotodiodo/ArancioBodeGuadagno10k.png}
    }
    \caption{Guadagno calcolato con l'\texttt{LO 3336}}
    \label{sett07:rd98nFUS}
\end{figure}


Abbiamo poi riacquisito anche la caratteristica IV del led arancione.

\begin{acquis}{Traccia\_Vin\_Vout\_diff.vi}
Fondo Scala AI0 & 10V\\
Fondo Scala AI1 & 10V\\
$R$ & $\SI{0.470}{k\Omega}$\\
Vmin & 0.0V \\
Vmax & 10.0V \\
Numero misure & 400 \\
\hline
LED & LO3336\\
\end{acquis}

\begin{figure}[H]
    \centering
    \subfloat[][Scala lineare]{
    \includegraphics[width=0.48\textwidth]{sett07/LEDandFotodiodo/LO3336_IV_linlin.png}
    }
    \subfloat[][Scala semilogaritmica]{
    \includegraphics[width=0.48\textwidth]{sett07/LEDandFotodiodo/LO3336_IV_linlog.png}
    }
    \caption{Caratteristica IV dell'\texttt{LO 3336}}
    \label{sett07:rd46nfUS}
\end{figure}

In sostanza, il valore che noi otteniamo con queste acquisizioni, è dello stesso ordine di grandezza di quanto dichiarato, e ci risulta essere di circa $\SI{0.06}{A/W}$.

Abbiamo poi osservato il fenomeno del \textit{gain ringing}, descritto anche nel documento della Hamamatsu. La nostra osservazione usa come sorgenti di impulsi il led alimentato a 2kHz dal generatore di funzioni. Riportiamo grafico e acquisizione.

\begin{acquis}{acquis\_base\_6x21.vi}
Fondo Scala [V] & 10 \\
Input terminal configuration & RSE \\
sample mode & finite samples \\
durata (s) & 0.001 \\
campioni al secondo & 250000\\
frequenza del generatore & 2kHz\\
ampl generatore & 6V\\
offset generatore & 3V\\
$R\ped{trans}$ & 100k$\Omega$\\
\end{acquis}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{sett07/LEDandFotodiodo/GainRinging.png}
    \caption{Fenomeno del gain ringing osservato}
    \label{sett07:pcsvb45x}
\end{figure}


MANCA IL 25: FARE L'ACQUISIZIONE CON IL CAPACITORE IN PARALLELO E IL GENERATORE CHE SPINGE IL LED CON UNA QUADRA. VEDERE SE LA COSA PERSISTE.

\section{Fotoresistenza}
\begin{exlist}
    \ok Es. 26
    \ok Es. 27
    \ok Hw. 7
    \ok Hw. 8
    \ok Hw. 9
    \ok Es. 28
    \todo Es. 29
    \todo Es. 30
\end{exlist}

Resistenza alla luce ambientale: $(4.5\pm0.5)\mathrm{kOhm}$.
Oscurato con un panno degli occhiali e sopra due mani: oltre $\SI{60}{MOhm}$. Solo il panno degli occhiali $(20\pm5)\mathrm{MOhm}$. Nel foglio delle specifiche della fotoresistenza, nella sezione \textit{Dark resistance} è riportato che il valore tipico della resistenza in condizioni di buio rientra nel range compreso tra $\SI{500}{k\Omega}$ e $\SI{20}{M\Omega}$.

Facendo i conti (mettere i conti) risulta che l'illuminazione della stanza è pari a $\SI{115}{lux}$, rispetto ai 200 misurati. (Nell'ipotesi che stia sulla retta).

Vediamo ora alcuni circuiti applicativi che utilizzano le fotoresistenze.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.68\textwidth]{sett07/Fotoresistenza/Application.png}
	\caption{Circuiti applicativi con fotoresistenze}
	\label{sett07:GXt5ipcC}
\end{figure}

\begin{comment}
	\textbf{Analog Applications}
	\begin{itemize}
		\item Camera Exposure Control
		\item Auto Slide Focus - dual cell
		\item Photocopy Machines - density of toner
		\item Colorimetric Test Equipment
		\item Densitometer
		\item Electronic Scales - dual cell
		\item Automatic Gain Control - modulated light source
		\item Automated Rear View Mirror
	\end{itemize}
	\textbf{Digital Applications}
	\begin{itemize}
		\item Automatic Headlight Dimmer
		\item Night Light Control
		\item Oil Burner Flame Out
		\item Street Light Control
		\item Absence / Presence (beam breaker)
		\item Position Sensor
	\end{itemize}
\end{comment}


Sotto riportiamo lo schema di un circuito effettuato con TINA di un LED che si accende quando l'illuminazione della fotoresistenza scende sotto un certo valore. Con FR è indicata la fotoresistenza, con R la resistenza che, come discusso in precedenza, deve essere superiore a $330/2=\SI{150}{\Omega}$, per rispettare la corrente massima che può fluire attraverso il LED, ad esempio $R=\SI{470}{\Omega}$.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.18\textwidth]{sett07/Fotoresistenza/Hw8.png}
    \caption{Circuito}
    \label{sett07:u36DCYbH}
\end{figure}

\subsection{Comparatore}
Possiamo costruire un circuito che alimenti il LED se l'illuminazione ambientale è oltre una determinata soglia impostabile.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.52\textwidth]{sett07/Fotoresistenza/Comparatore.png}
    \caption{Comparatore}
    \label{sett07:7DCjPYyA}
\end{figure}

\subsection{Misura della caratteristica \textit{resistenza illuminazione}}

Nel circuito indicato dal testo abbiamo scelto $R=\SI{1.2}{k\Omega}$. Abbiamo infatti che in condizioni ambientali la fotoresistenza vale all'incirca $\SI{4.5}{k\Omega}$, mentre all'esposizione della luce di un led all'incirca $\SI{0.4}{k\Omega}$, per questo motivo si è scelto un valore in questo intervallo. Per quanto riguarda l'incertezza sulla fotoresistenza potremmo propagare l'incertezza tramite l'espressione:
\begin{equation}
    FR=R\frac{V_\mathrm{in}-V_\mathrm{out}}{V_\mathrm{out}}
\end{equation}
Tuttavia l'incertezza sulla misura di $V_\mathrm{in}$ e $V_\mathrm{out}$ attraverso la scheda DAQ National è abbastanza piccola da poter attribuire a FR la stessa incertezza di R, ovvero l'1\%.

\begin{comment}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.6\textwidth]{sett07/Fotoresistenza/es30.png}
		\caption{Grafico valore di fotoresistenza vs corrente che scorre nel LED}
		\label{sett07:rRCAO02A}
	\end{figure}
\end{comment}