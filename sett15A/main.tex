\chapter{Misuratore di capacità}

\section{Obiettivi/abstract}
Questa esperienza si pone due obiettivi:
\begin{itemize}
	\item Misurare la capacità di un condensatore a lastre piane e parallele per poter poi trarre una stima di $\varepsilon_0$, la costante dielettrica del vuoto.
	\item Misurare per una soluzione diluita di \texttt{NaCl} la costante dielettrica relativa al variare della concentrazione del sale. In particolare, abbiamo cercato di stimare il coefficiente $\alpha$ all'equazione \ref{sett15A:fc7Bnlkn}.
\end{itemize}
\section{Cenni teorici}
\subsection{Variazione di $\varepsilon_r$ in funzione della concentrazione salina}
È noto che la costante dielettrica relativa $\varepsilon_r$ nel caso di una soluzione diluita (meno di $\SI2M$)\footnote{J. B. Hasted, D. M. Ritson,  and C. H. Collie, J. Phys. Chem. 16 (1948).} vale:
\begin{equation}\label{sett15A:fc7Bnlkn}
	\varepsilon_r = \varepsilon_{r,0} (1- \alpha M)
\end{equation}

È ragionevole supporre che l'andamento della capacità di una faccia di un condensatore a facce piane e parallele, la cui altra piastra è collegata a terra, sia lineare nella costante dielettrica:
\[
	C = C_0 + \varepsilon_r C_1
\]
dove $\varepsilon_r$ è la costante dielettrica relativa del dielettrico posto fra le piastre del condensatore.

Combinando le due equazioni prededenti si ha che:
\begin{equation}\label{sett15A:ZVcenlkn} 
	C = C_0 + C_1 \varepsilon_{r,0} \left(1- \alpha M\right)
\end{equation}
\subsection{Due \textit{layer} di dielettrico}
Supponiamo ora che le due piastre non siano divise da un solo dielettrico, ma da due \textit{layer} di diverso materiale, \textit{ad esempio} plastica (costante dielettrica relativa $\varepsilon_{Pl}$) e acqua salata (costante dielettrica relativa $\varepsilon_{H_2O, \,x\,\mathrm{mol/L\, NaCl}}$), con spessore rispettivamente $d_{Pl}$ e $d_{H_2O}$. Se $C_1$ è la capacità nel vuoto legata all'interazione fra le due piastre:
\begin{equation}
C = C_0 + C_1\frac{d}{d_{Pl}/\varepsilon_{Pl} + d_{H_2O}/\varepsilon_{H_2O, \,x\,\mathrm{mol/L\, NaCl}}}
\end{equation}
\begin{equation}
\Rightarrow C = C_0 + C_1\frac{d}{d_{Pl}/\varepsilon_{Pl} + d_{H_2O}/[\varepsilon_{H_2O}\left(1-\alpha M\right)]}
\label{sett15A:vsdvijd3}
\end{equation}

\section{\textit{Setup} sperimentale}
\subsection{Matrice di capacità: AD7747 \textit{vs} AD7745}
Sono state effettuate misure della capacità con l'integrato AD7747, collegato al computer tramite un Arduino Due, per misurare la capacità rispetto a terra di una delle due piastre, collegando l'altra a terra. Sarebbe stato possibile anche usare l'AD7745 per misurare la capacità tra le due piastre, con il vantaggio di non risentire di capacità parassite dovute all'interazione con l'ambiente esterno.
In realtà però l'interazione fra i due cavi è comunque rilevante, tale da non rendere comunque riproducibile l'esperimento, per cui abbiamo preso l'AD7747, così da misurare anche la capacità della singola piastra.
\begin{figure}[H]
	\centering
	\subfloat[][Diagramma a blocchi dell'AD7745] {\includegraphics[width=0.47\textwidth]{sett15A/images/7745BlockDiagram.png}}
	\hspace{10pt}
	\subfloat[][Diagramma a blocchi dell'AD7747] {\includegraphics[width=0.47\textwidth]{sett15A/images/7747BlockDiagram.png}}
	\caption{Diagrammi a blocchi di due capacimetri disponibili}
	\label{sett15A:cusacome}
\end{figure}
\subsection{Apparato sperimentale}

Abbiamo quindi costruito un apparato sperimentale con l'ausilio di supporti di cartone in modo da non dover toccare nulla (se non che per rimuovere soluzioni saline o versarne tramite una pipetta) durante le misurazioni e ottenere così risultati coerenti all'interno della sessione di acquisizione: la riproducibilità è quindi da intendersi in senso lato: è improbabile ottenere valori compatibili di capacità fra diversi setup sperimentali, ma i risultati del fit ($\alpha$, $\varepsilon_0$, $\varepsilon_r$) dovrebbero essere compatibili.

Abbiamo misurato la capacità tra due lastre piane, poste su facce opposte di una cuvetta: le due piastre misurano $\SI{1.2}{cm}\times\SI{4.5}{cm}$ e sono distanti $\SI{1.2}{cm}$. La cuvetta ha dei bordi di plastica spessi $\SI{0.1}{cm}$, e ha uno spazio interno rimanente per l'acqua di dimensioni $\SI{1.0}{cm}\times\SI{1.0}{cm}\times\SI{4.4}{cm}$.

Una piastra è stata collegata al channel 1 e l'altra piastra è collegata a terra. Il cavo che collega la piastra al channel 1 è schermato: tale schermatura è collegata al pin SHIELD/EXC del 7747, in modo che la carica che si accumula sul filo sia pressocché nulla e questo non contribuisca alla determinazione della capacità. 

Sono state preparate le soluzioni di cloruro di sodio, per diverse molarità ($\SI{0.0}{M}$, $\SI{0.2}{M}$, $\SI{0.4}{M}$, $\SI{0.6}{M}$, $\SI{0.8}{M}$, $\SI{1.0}{M}$). Come già appuntato, per non modificare la posizione dei cavi è stata impiegata una pipetta Pasteur per riempire e svuotare la cuvetta perchè lo spostamento dei cavi e quindi una loro differente configurazione sperimentale comporta differenze nella misurazione non trascurabili. 
\section{Dati raccolti}
 Riportiamo in tabella i valori delle varie misure, con la cuvetta vuota e per divese concentrazioni.

\begin{table}[H]
\begin{center}
	\begin{tabular}{lll}
		\toprule
		mol[M] 	   & C[pF]	  & StdDev[pF]	   \\ \midrule
		vuoto      & 7.592    & 0.006      \\
		0.0        & 10.732   & 0.007      \\
		0.2        & 10.725   & 0.006      \\
		0.4        & 10.712   & 0.006      \\
		0.6        & 10.684   & 0.006      \\
		0.8        & 10.673   & 0.006      \\
		1.0        & 10.642   & 0.006      \\ \bottomrule
	\end{tabular}
\end{center}
\end{table}


\section{Analisi dei dati}
Per poter fittare la funzione \ref{sett15A:vsdvijd3} e quindi ricavare il valore di $\alpha$ è pratico avere a priori una stima di $C_0$ e $C_1$. Per farlo, risolviamo il sistema che si ottiene considerando la misura della capacità a vuoto, $A$, e con acqua distillata, $B$, usando $\varepsilon_{Pl}=2.2\pm0.2$ come valore tipico di permittività relativa delle plastiche, e $\varepsilon_{H_2O}\approx80$ per l'acqua.
\[
\begin{cases}
C_0 + C_1\frac{d}{d_{Pl}/\varepsilon_{Pl}+d_{H_2O}} \approx C_0 + 1.1\,C_1\approx A\\
C_0 + C_1\frac{d}{d_{Pl}/\varepsilon_{Pl}+d_{H_2O}/\varepsilon_{H_2O}} \approx C_0 + 11.6\,C_1\approx B
\end{cases}
\]
Da cui si ottiene:
\begin{equation}
C_1=\frac{B-A}{11.6}\approx\SI{0.30\pm0.03}{pF}\quad ;\quad C_0 = A-1.1\,C_1\approx\SI{7.26\pm0.03}{pF}
\end{equation}

Da questi risultati possiamo stimare il valore di $\varepsilon_0$:
\begin{equation}
\varepsilon_0 = C_1 \frac{d}{A} = (6.7\pm1.2)\,\SI{}{pF/m}
\end{equation}

Otteniamo poi una stima del parametro $\alpha$ tramite un \textit{best fit} della funzione \ref{sett15A:vsdvijd3}, utilizzando come parametro libero proprio $\alpha$.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{sett15A/images/easyfit.png}
	\caption{Dati acquisiti e curva di \textit{best fit}}    \label{sett15A:X8T8GcC8}
\end{figure}
La stima ottenuta vale:
\[
\alpha = (0.190 \pm 0.007)\,\mathrm{L/mol}
\]
\section{Conclusioni}

La stima di $\varepsilon_0$ ottenuta non è compatibile con il valore CODATA attuale di circa $8.854\,\SI{}{pF/m}$. Due possibili fonti di problemi sono il fatto che le piastre ai bordi della cuvetta sono lungi dall'essere un condensatore ideale a facce piane e parallele e il fatto che la stima di $C_1$ dipende significativamente dal valore di $\varepsilon_{Pl}$, di cui abbiamo preso un valore medio fra diversi tipi di plastiche in vendita.

Il valore di $\alpha$ è compatibile con il valore di $0.19 \,\mathrm{L/mol}$ presente in letteratura\footnote{\url{https://pubs.acs.org/doi/pdf/10.1021/jp971623a}}.

\section{Script utilizzato}

Abbiamo usato il seguente codice per arduino.
\begin{minted}{arduino}
#include <Wire.h>
#define AD 0x48
#define CAPDAC_FACTOR 269.84

byte outbuf[10];
byte capdac;

void setup()
{
	capdac = 0;
	Serial.begin(115200);
	Wire.begin(); 
	Wire.beginTransmission(AD);
	Wire.write(0x07);  //registro CAP
	Wire.write(0x80); //Enable CAP, CAPCHOP=0
	Wire.write(0x09); //scegli EXC
	Wire.write(0x0C); //scegli EXCEN,EXCAPDAC
	Wire.endTransmission();
	delay(200);
	Wire.beginTransmission(AD);
	Wire.write(0x0A);//registro configurazione
	Wire.write(0b00010001);//conversione singola, ~220 ms
	Wire.endTransmission();
	delay(200);
}

void loop()
{ 
	long int i;
	long int datalen;
	if (Serial.available()>0) {
		datalen = Serial.parseInt();
		while (Serial.available()>0)
		Serial.read();
		for (i=0;i<datalen;i++) {
			
		}
		for(i=0;i<datalen+15;i++) {
			if (capdac>0x3F)
			capdac = 0x3F;
			else if (capdac<0)
			capdac = 0;
			Wire.beginTransmission(AD);
			Wire.write(0x0B);//registro configurazione
			Wire.write(0b10000000+capdac);//conversione singola, ~220 ms
			Wire.endTransmission();
			long int x_val;
			double cap;
			int nb, c;
			nb=4; //numero byte richiesti
			c=0;  //contatore byte
			delay(50); 
			Wire.requestFrom(AD,nb);
			while(Wire.available())
			{ 
				outbuf[c++] = Wire.read();
					//accumula i byte letti nel vettore outbuf
			}
			//i byte letti 1-2-3 sono i byte che compongono la lettura di C
			x_val=outbuf[1];
			x_val <<= 8;
			x_val += outbuf[2];
			x_val <<= 8;
			x_val += outbuf[3];
			cap = CAPDAC_FACTOR*capdac + (x_val-0x7FFFFF)/1024.0;
				// conversione in fF
			if (i>=15)
			Serial.println(cap, 3);
			if (outbuf[1]==0xFF)
			capdac += 5;
			else if (outbuf[1]==0x00)
			capdac -= 5;
		}
	} else delay(10);
}
\end{minted}

Avevamo poi bisogno di usare un programma per acquisire i dati dal PC, qualcosa più pratico del serial monitor. E, visto che nel corso di TD non si può usare Python, abbiamo usato C++, servendoci della libreria \href{serial}{https://github.com/wjwwood/serial}:
\begin{minted}{c++}
#include <serial/serial.h>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <unistd.h>
#include <cstdlib>

using namespace std;
using namespace serial;

int main(int argc, char * argv[]) {
	if (argc!=3) {
		cout << "Error: argc is " << argc << " instead of 2." << endl;
		exit(-1);
	}
	string port("/dev/");
	port += argv[1];
	Timeout tout = Timeout::simpleTimeout(2000); //ms
	try {
		Serial ser(port, 115200, tout);
		usleep(200000); // perché serial è rotto
			// Su arduino più lenti del Due potrebbe servire più tempo
		cout << "\nSerial config'd to port " << port << endl;
		ser.write(argv[2]);
		ser.write("\n");
		cout << "Asked to return " << argv[2] << " samples." << endl;
		
		char status_buffer[100];
		vector<string> misure_str;
		string nuova_misura = ser.readline();
		while (nuova_misura != "") {
			misure_str.push_back(nuova_misura);
			nuova_misura = ser.readline();
			if (misure_str.size()%20==0) {
			sprintf(status_buffer, "%.2f%%      ", \
			100.0*misure_str.size()/atoi(argv[2]));
			cout << status_buffer << "\r" << flush;
			}
		}
		vector<double> misure;
		for (int i=0; i<misure_str.size(); i++)
			misure.push_back(stof(misure_str[i]));
		double sum = 0, chi = 0;
		for (int i=0; i<misure.size(); i++)
			sum = sum + misure[i];
		double avg = sum/misure.size();
		for (int i=0; i<misure.size(); i++)
			chi = chi + pow(misure[i]-avg, 2);
		double std_dev = sqrt(chi/misure.size());
		cout << "\n\nAverage: " << avg << endl;
		cout << "Std dev: " << std_dev << endl;
		cout << "Mean std dev: " \
			<< std_dev/sqrt((float) misure.size()) << endl;
	} catch (serial::IOException e) {
		cout << "IOException at opening the serial port. Possible cause: \
			verify that " << port << " is a valid serial port." << endl;
		exit(-3);
	} catch (...) {
		cout << "Unknown error at opening the serial port." << endl;
		exit(-2);
	}
	return 0;
}
\end{minted}