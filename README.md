# TDlogbook
[![build status](https://gitlab.com/1998marcom/tdlogbook/badges/master/pipeline.svg)](https://gitlab.com/1998marcom/tdlogbook/-/jobs/artifacts/master/raw/LogbookFalcoMalandrone.pdf?job=build)

Logbook di TD di Alessandro Falco e Marco Malandrone. Ultimo PDF scaricabile [qui](https://gitlab.com/1998marcom/tdlogbook/-/jobs/artifacts/master/raw/LogbookFalcoMalandrone.pdf?job=build)

![CC-BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png "CC-BY-NC-SA")
(salvo ove espressamente diversamente indicato)