\chapter{Misuratore di campo magnetico}

\section{Verifica della legge sul campo di dipolo con un piccolo supermag}
\subsection{Obiettivo}
L'obiettivo della prima parte dell'esperienza consiste in una parziale verifica delle leggi sul campo di dipolo. In particolare ci proponiamo di:
\begin{itemize}
	\item verificare l'andamento come $1/r^3$ del campo magnetico in funzione della distanza dal dipolo $r$, senza variare l'orientazione relativa tra la congiungente dipolo-sensore e l'asse del dipolo;
	\item verificare che l'intensità del campo magnetico si dimezza se, mantenendo fissa la distanza $r$ dal sensore, si orienta in dipolo perpendicolarmente alla congiungente con il sensore piuttosto che parallelamente.
\end{itemize} 
\subsection{Cenni teorici}
Nel limite di dipolo ideale, ovvero nel caso in cui le dimensioni del nostro magnete possano essere considerate trascurabili rispetto alle lunghezze in gioco, possiamo scrivere il campo generato da un dipolo di momento magnetico $\textbf{m}$ come:
\begin{equation}\label{sett16A:2f7PDbFB}
\mathbf{B}({\mathbf{r}})=\frac{\mu_{0}}{4\pi}\left[\frac{3\mathbf{\hat{r}}(\mathbf{m}\cdot\mathbf{\hat{r}})-\mathbf{m}}{r^{3}}\right]
\end{equation}
\subsection{Raccolta dati}
Abbiamo collegato il sensore MLX90393 all'arduino nano e abbiamo adattato \href{https://github.com/adafruit/Adafruit\_MLX90393\_Library/}{lo script fornito dalla Adafruit} per acquisire i dati.

Abbiamo utilizzato un supporto in legno per poter regolare la distanza tra supermag e sensore, e acquisire così una tabella dei dati che ragistrasse l'andamento dell'intensità del campo magnetico al variare della distanza tra il dipolo e il sensore. In questo caso abbiamo usato un magnete di piccole dimensioni, circa $\SI{1}{cm}\times \SI{0.4}{cm}\times \SI{0.4}{cm}$.

\begin{table}[H]
	\centering
	\begin{tabular}{cc}
		\toprule
		d[cm]	& B[$\mu$T]		\\ \midrule
		3.7		& 2520.63		\\
		4.4		& 1560.61		\\
		6.2		& 527.803		\\
		7.4		& 296.109		\\
		8.7		& 230.733		\\
		10.4    & 121.054		\\
		11.2	& 138.325		\\
		13.4	& 86.2005		\\
		13.7	& 101.613		\\
		vuoto	& 40.2957		\\ \bottomrule
	\end{tabular}
	\caption{Dati acquisiti: un supermag di piccole dimensioni puntato verso il sensore}
	\label{sett16A:dsjindsv}
\end{table}

Abbiamo poi provato a verificare che il campo di dipolo, a parità di distanza dalla sorgente, fosse circa la metà nella condizione in cui il dipolo è perpendicolare alla congiungente dipolo-sensore piuttosto che parallelo a questa direzione.
\begin{table}[H]
	\centering
	\begin{tabular}{ccc}
		\toprule
		d[cm]	& Config. Perpendicolare B[$\mu$T]	& Config. Parallela  B[$\mu$T]	\\ \midrule
		8.7		& 61.9555			& 130.066			\\
		13.4	& 42.8773			& 51.4238			\\ \hline
		vuoto	& 39.2923			&					\\ \bottomrule
	\end{tabular}
	\caption{Dati acquisiti: un geomag di piccole dimensioni orientato in due modi diversi rispetto alla direzione geomag-sensore}
	\label{sett16A:OAjindsv}
\end{table}
Nel primo caso otteniamo dei valori compatibili con l'andamento atteso, mentre nel secondo caso la misura è vicina al rumore di fondo, e non è quindi da considerarsi come attendibile.

\subsection{Analisi dati}

Eseguendo il fit dell'andamento del campo magnetico di un dipolo ideale in funzione della distanza $d$ dal sensore, tramite la funzione
\begin{equation}
\mathbf{B}(d)=\frac{\mu_{0}}{4\pi}\left[\frac{2 m}{d^{3}}\right]
\label{sett16A:csdin39n}
\end{equation}
abbiamo trovato che il momento di dipolo magnetico associato al supermag è 
\[m=(645\pm8)\times10^{-3}\mathrm{m^2A}\]
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{sett16A/images/fit01.png}
	\caption{Dati raccolti e curva di best-fit secondo il modello dell'equazione \ref{sett16A:csdin39n}}    \label{sett16A:7pJz7ttk}
\end{figure}

\subsection{Conclusioni}
Entrambi i set di dati sono compatibili con la legge assunta per descrivere il fenomeno.

\section{Pseudo-solenoide realizzato concatenando geomag}
\subsection{Obiettivo}
Nella seconda parte dell'esperienza ci proponiamo di verificare la relazione tra la lunghezza di un solenoide e il campo magnetico da esso generato, verificando contestualmente anche il fatto che una catena di geomag possa essere approssimata come un solenoide.

\subsection{Cenni teorici}
Si può ricavare che sull'asse di simmetria di un solenoide di lunghezza $l$, di raggio $R$, con $N$ spire e parcorso da un'intensità di corrente $I$, la componentre radiale del campo magnetico è nulla, mentre la componente assiale è pari a \footnote{\url{https://en.wikipedia.org/wiki/Solenoid\#Finite\_continuous\_solenoid}}:
\begin{equation}\label{sett16A:NZxQnlyN}
B_z = \frac{\mu_0 NI}{2}\Biggl( \frac{l/2-z}{l \sqrt{R^2+(l/2-z)^2}} + \frac{l/2+z}{l \sqrt{R^2+(l/2+z)^2}}\Biggr)
\end{equation}
dove abbiamo indicato con $z$ la distanza dl centro del solenoide.

\subsection{Raccolta dati}
Abbiamo collegato il sensore MLX90393 all'arduino nano e abbiamo adattato \href{https://github.com/adafruit/Adafruit\_MLX90393\_Library/}{lo script fornito dalla Adafruit} per acquisire i dati.

Abbiamo concatenato diversi geomag (di dimensioni circa $\SI{2.6}{cm}\times \SI{0.7}{cm}\times \SI{0.7}{cm}$), mantenendo fissa la distanza del geomag più vicino dal sensore, e facendo in modo che il sensore fosse posizionato sull'asse dello pseudo-solenoide:

\begin{table}[H]
	\centering
		\begin{tabular}{cc}
			\toprule
			n geomag	& B[$\mu$T]		\\ \midrule
			1			& 1598.89		\\
			1			& 2177.2		\\
			3			& 2414.32		\\
			4			& 2579.29		\\
			5			& 2663.8		\\
			6   		& 2713.24		\\
			7			& 2743.6		\\
			8			& 2770.79		\\
			9			& 2779.81		\\
			vuoto		& 58.2949		\\ \bottomrule
		\end{tabular}
	\caption{Intensità del campo magnetico al variare del numero di geomag utilizzati}
\label{sett16A:ISjindsv}
\end{table}


\subsection{Analisi dei dati}
Avendo assimilato i nostri dipoli magnetici a solenoidi di lunghezza finita, possiamo fittare la relazione \ref{sett16A:NZxQnlyN} riscritta come
\[
B_z = nA\Biggl( \frac{nC/2-z}{nC \sqrt{B+(nC/2-z)^2}} + \frac{nC/2+z}{nC \sqrt{B+(nC/2+z)^2}}\Biggr)
\]
con i 3 parametri liberi A, B e C.

Considerando che i geomag sono alti $(2.65\pm0.06)\mathrm{cm}$ e che il primo geomag è stato posto $\SI{4.4}{cm}$ sopra il sensore, chiamando $n$ il numero di geomag sovrapposti, z vale:
\[z=(4.4+n/2)\,\mathrm{cm}\]
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{sett16A/images/fit03Cusu.png}
	\caption{Grafico con dati raccolti e curva di best-fit secondo il modello dell'equazione \ref{sett16A:NZxQnlyN}}    \label{sett16A:WOJU4xn2}
\end{figure}
Avendo usato un'incertezza di $\SI{20}{\mu T}$, pari alla metà del rumore di fondo ($\sim$ 40), il chi quadro ottenuto è di 1.43, con un p-value associato di 0.036, che indica un fit quasi fin troppo buono.
\subsection{Conclusioni}
Il set di dati è (perfettamente) compatibile con la legge assunta per descrivere il fenomeno.